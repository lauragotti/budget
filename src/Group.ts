import { Expense, Person } from "./entities";

export class Group {
  persons: Person[] = []
  expenses: Expense[] = []
  div: HTMLDivElement = document.createElement("div");
  formDiv: HTMLDivElement = document.createElement("div");
  expensesDiv: HTMLDivElement = document.createElement("div");
  form: HTMLFormElement;
  owesDiv: HTMLDivElement = document.createElement("div");

  constructor(persons: Person[] = []) {
    this.persons = persons
    this.div.appendChild(this.formDiv);
    this.div.appendChild(this.expensesDiv)
    this.div.appendChild(this.owesDiv);
    this.form = this.createExpenseForm();
    this.formDiv.appendChild(this.form);
  }

  render() {
    return this.div
  }

  createExpenseForm(): HTMLFormElement {
    const form = document.createElement("form");

    const nameLabel = document.createElement("label");
    nameLabel.textContent = "Expense Name: ";
    const nameInput = document.createElement("input");
    nameInput.type = "text";
    nameInput.required = true;
    nameLabel.appendChild(nameInput);

    const amountLabel = document.createElement("label");
    amountLabel.textContent = "Expense Amount: ";
    const amountInput = document.createElement("input");
    amountInput.type = "number";
    amountInput.step = "0.01";
    amountInput.min = "0.01";
    amountLabel.appendChild(amountInput);

    const spenderLabel = document.createElement("label");
    spenderLabel.textContent = "Expense Spender: ";
    const spenderSelect = document.createElement("select");

    this.persons.forEach(person => {
      const option = document.createElement("option");
      option.value = person.name;
      option.textContent = person.name;
      spenderSelect.appendChild(option);
    });

    spenderLabel.appendChild(spenderSelect);

    const addButton = document.createElement("button");
    addButton.textContent = "Add Expense";

    form.appendChild(nameLabel);
    form.appendChild(amountLabel);
    form.appendChild(spenderLabel);
    form.appendChild(addButton);

    form.addEventListener("submit", (e) => {
      e.preventDefault();
      const name = nameInput.value;
      const amount = Number(amountInput.value);
      const spenderName = spenderSelect.value;

      const spender = this.persons.find(person => person.name === spenderName);
      if (spender) {
        const expense: Expense = {
          name: name,
          amount: amount,
          spender: spender,
        };
        this.addExpense(expense);
        nameInput.value = "";
        amountInput.value = "";
      }
    });

    return form;
  }

  addExpense(expense: Expense) {
    this.expenses.push(expense)
    this.renderExpenses();
    this.calculateOwes(this);
    this.renderTotalExpenses();
  }

  renderExpenses() {
    this.expensesDiv.innerHTML = "Expenses :";
    let display: string[] = [];
    for (const item of this.expenses) {
      display.push(item.name + " " + String(item.amount) + "€ - " + item.spender.name)
    }
    for (const item of display) {
      const line = document.createElement("p");
      line.textContent = item;
      this.expensesDiv.append(line)
    }
    return this.expensesDiv
  }

  calculateOwes(group: Group) {
    // Initialize an object to store the balances
    const balances: { [key: string]: number } = {};

    // Calculate individual expenses
    group.expenses.forEach(expense => {
      const spender = expense.spender.name;
      if (!balances[spender]) {
        balances[spender] = 0;
      }
      balances[spender] += expense.amount;
    });

    // Calculate the average expense
    const totalExpenses = Object.values(balances).reduce((total, amount) => total + amount, 0);
    const averageExpense = totalExpenses / group.persons.length;

    // Calculate who owes and is owed
    const summary: string[] = [];
    for (const person of group.persons) {
      const name = person.name;
      if (!balances[name]) {
        balances[name] = 0;
      }
      const balance = balances[name] - averageExpense;
      if (balance > 0) {
        summary.push(`${name} is owed ${balance.toFixed(2)}€`);
      } else if (balance < 0) {
        summary.push(`${name} owes ${Math.abs(balance).toFixed(2)}€`);
      }
    }
    this.renderOwes(summary);
    return summary
  }

  renderOwes(summary: string[]) {
    this.owesDiv.innerHTML = "Balance :";
    for (const item of summary) {
      const line = document.createElement("p");
      line.textContent = item;
      this.owesDiv.append(line)
    }
  }

  totalExpenses(name: string) {
    let total = 0;
    for (const expense of this.expenses) {
      if (expense.spender.name === name) {
        total += expense.amount;
      }
    }
    return total;
  }

  renderTotalExpenses() {
    const div = document.createElement("div")
    this.owesDiv.appendChild(div);

    const form = document.createElement("form")
    const spenderLabel = document.createElement("label");
    spenderLabel.textContent = "Total expenses of ";
    const spenderSelect = document.createElement("select");

    this.persons.forEach(person => {
      console.log(person)
      const option = document.createElement("option");
      option.value = person.name;
      option.textContent = person.name;
      spenderSelect.appendChild(option);
    });

    spenderLabel.appendChild(spenderSelect);

    const totalButton = document.createElement("button");
    totalButton.textContent = "Check total";
    form.appendChild(spenderLabel);
    form.appendChild(totalButton);

    div.appendChild(form)
    const result = document.createElement("p")

    div.appendChild(result)
    form.addEventListener("submit", (e) => {
      e.preventDefault();

      result.textContent = " ";
      const spenderName = spenderSelect.value
      const total = this.totalExpenses(spenderName)
      result.textContent = String(total) + "€";

    })

  }

}