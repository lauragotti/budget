import { Group } from './Group'
import { Person } from './entities';
import './style.css'

// const persons: Person[] = [
//   { name: "Laura" },
//   { name: "Julie" },
//   { name: "Lina" },
// ];

// let group = new Group(persons)

// console.log(group)

// const expense1 = {
//   name: "truc",
//   amount: 25,
//   spender: persons[0]
// }
// const expense2 = {
//   name: "machin",
//   amount: 10,
//   spender: persons[1]
// }

// group.addExpense(expense1)
// group.addExpense(expense2)

// console.log(group)


const app = document.querySelector("#app")
const groupDiv = app?.querySelector("Group");

// create group
let groupPersons: Person[] = [];
const createDiv = document.createElement("div");
app?.append(createDiv);

const form = document.createElement("form");

const namesLabel = document.createElement("label");
namesLabel.textContent = "Enter Names (separé avec espace):";
const namesInput = document.createElement("input");
namesInput.type = "text";
namesInput.id = "namesInput";
namesInput.required = true;

const addButton = document.createElement("button");
addButton.type = "submit";
addButton.textContent = "Create group";

form.addEventListener("submit", function (event) {
  event.preventDefault();
  const names = namesInput.value;
  if (names) {
    groupPersons = [];
    const splitNames = names.split(' ');
    for (const item of splitNames) {
      const line: Person = {
        name: item
      }
      groupPersons.push(line);
    }
    let newGroup = new Group(groupPersons);
    groupDiv?.replaceWith(newGroup.render());
    createDiv.style.display = "none";

  }
});

form.appendChild(namesLabel);
form.appendChild(namesInput);
form.appendChild(addButton);

createDiv.appendChild(form);
