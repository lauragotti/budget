export interface Person {
  name: string
}

export interface Expense {
  name: string,
  amount: number, 
  spender: Person
}